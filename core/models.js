`use strict`
const fs = require(`fs`)

module.exports = () => {
    return new Promise(async (resolve, reject) => {
        let models = {}
        const modelFiles = fs.readdirSync(`${__basedir}/app/models`)

        for await (let file of modelFiles) {
            const name = file.replace(/.js|.ts/g, ``).toLowerCase()
            const src  = require(`${__basedir}/app/models/${file}`)

            models[name] = src
        }
        
        global.models = models
        resolve(models)
    })
}