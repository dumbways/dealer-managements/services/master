`use strict`
const { Validator: v } = require('node-input-validator')
const { validate, commit, hashids } = require(`../helper/helper`)
const fs = require('fs');

module.exports = {
    async list(req, res) {
        let cars = await db.car.findAll({
            attributes: ['id', 'car_name', 'brand_id', 'color', 'description', 'stock', [sequelize.col('mb.brand_name'), 'brand_name'] ],
            include : [{ model : db.m_brand, as: 'mb', attributes : [], require : true }],
            where: { statusid: 1 },
            raw: true
        })
        let brands = await db.m_brand.findAll({
            attributes: ['id', 'brand_name' ],
            where: { statusid: 1 },
            raw: true
        })
        cars = hashids.encodeArray(cars, ['id', 'brand_id'])
        brands = hashids.encodeArray(brands, ['id'])
        res.json({ status: 1, data: { cars: cars, brands: brands } })
    },
    async add(req, res) {
        let validator = new v( { ...req.body, ...req.files }, {
            car_name: 'required',
            brand_id: 'required',
            color: 'required',
            description: 'required',
            stock: 'required',
            image: 'required',
            'image.mimetype': 'in:image/jpg,image/jpeg,image/png'
        })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)

        /** Save File (Image) */
        let filename = req.files.image.filename.split('.')
        let imageName = `${req.body.car_name}_${req.body.brand_id}.${filename[filename.length - 1]}`
        req.body.brand_id = hashids.decode(req.body.brand_id)
        fs.rename(req.files.image.file, `${__basedir}/protected/cars/${imageName}`, async (err) => {
            /** Save Data */
            let save = await db.car.create({
                car_name: req.body.car_name,
                brand_id: req.body.brand_id,
                color: req.body.color,
                description: req.body.description,
                stock: req.body.stock,
                image: imageName,
                create_by: req.auth.username,
                statusid: 1
            })
            await validate(res, save, 'An error occured while saving car\'s data. Please try again.')
            res.json({ status: 1 })
        })
    },
    async del(req, res) {
        let validator = new v(req.params, { id: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)
        
        req.params.id = hashids.decode(req.params.id)
        let find = await db.car.findOne({
            attributes: ['id'],
            where: { id: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Cars is not found')

        let save = db.car.update(
            { statusid: 0 },
            { where: { id: req.params.id, statusid: 1 } }
        )
        await validate(res, save, 'An error occured while saving car\'s data. Please try again.')
        res.json({ status: 1 })
    },
    async view(req, res) {
        req.params.id = hashids.decode(req.params.id)
        let cars = await db.car.findOne({
            attributes: ['id', 'car_name', 'brand_id', 'color', 'description', 'stock', [sequelize.col('mb.brand_name'), 'brand_name'] ],
            include : [{ model : db.m_brand, as: 'mb', attributes : [], require : true }],
            where: { id: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, cars, 'Cars is not found')

        let brands = await db.m_brand.findAll({
            attributes: ['id', 'brand_name' ],
            where: { statusid: 1 },
            raw: true
        })

        cars = hashids.encodeObject(cars, ['id', 'brand_id'])
        brands = hashids.encodeArray(brands, ['id'])
        res.json({ status: 1, data: { cars: cars, brands: brands } })
    },
    async image(req, res) {
        req.params.id = hashids.decode(req.params.id)
        let cars = await db.car.findOne({
            attributes: [ 'image' ],
            where: { id: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, cars, 'Cars is not found')
        res.sendFile(`${__basedir}/protected/cars/${cars.image}`)
    }
    // async edit(req, res) {
    //     let validator = new v(req.body, { brand_name: 'required' })
    //     let matched = await validator.check()
    //     await validate(res, matched, validator.errors)
        
    //     req.params.id = hashids.decode(req.params.id)
    //     let find = await db.m_brand.findOne({
    //         attributes: ['id'],
    //         where: { id: req.params.id, statusid: 1 },
    //         raw: true
    //     })
    //     await validate(res, find, 'Brand is not found')
        
    //     let save = await sequelize.transaction()
    //     let deactive = db.m_brand.update(
    //         { statusid: 0 },
    //         { where: { id: req.params.id, statusid: 1 } }
    //     )
    //     let create = await db.m_brand.create({
    //         brand_name: req.body.brand_name,
    //         statusid: 1
    //     })
    //     await commit(save, deactive && create)
    //     await validate(res, deactive && create, 'An error occured while saving brand\'s data. Please try again.')
    //     res.json({ status: 1 })
    // },
}