`use strict`

module.exports = {
    async menu (rolename = []) {
        const menu = await db.s_menu.findAll({
            attributes : [ 'id', 'menu', 'parent_id', 'url', 'icons', 'sequence', [sequelize.col('s_label_menu.label'), 'label'] ],
            include : [{ model : db.s_label_menu, attributes : [], require : true }],
            where : { rolename : { [Op.overlap] : rolename }, statusid : 1 },
            order : ['sequence'],
            raw: true
        })
    
        let response = [], tempMenu = []
        
        const insertParent = (newMenu, menu) => {
            for (let x in menu) {
                if (menu[x]['id'] === newMenu['parent_id']) {
                    menu[x]['child'] = [
                        ... menu[x]['child'] ? menu[x]['child'] : [],
                        {
                            title       : newMenu.menu,
                            url         : newMenu.url,
                            sequence    : newMenu.sequence,
                            icons       : newMenu.icons
                        }
                    ]
                    break
                }
            }
        }
    
        for (let row of menu) {
            if (row.parent_id) {
                insertParent(row, tempMenu)
            } else {
                tempMenu = [ ... tempMenu, row]
            }
        }
    
        for (let row of tempMenu) {
            response[row.label] = {
                label : row.label,
                menu : [
                    ... response[row.label] ? response[row.label].menu : [],
                    {
                        title : row.menu,
                        icons : row.icons,
                        url : row.url,
                        sequence : row.sequence,
                        child : row.child ? row.child : undefined
                    }
                ]
            }
        }
    
        return { menu: Object.keys(response).map(row => response[row]) }
    },
    async userinfo (req, res) {
        let userinfo = await db.personal_data.findOne({
            attributes: ['username', 'name', 'address'],
            where: { username: req.auth.username, statusid: 1 },
            raw: true
        })

        userinfo = { ...userinfo, ... await this.menu(req.auth.roles) }

        res.json(userinfo)
    }
}