`use strict`
const { Validator: v } = require('node-input-validator')
const { validate, hashids } = require(`../helper/helper`)

module.exports = {
    async list(req, res) {
        let brands = await db.m_brand.findAll({
            attributes: ['id', 'brand_name' ],
            where: { statusid: 1 },
            raw: true
        })
        brands = hashids.encodeArray(brands, 'id')
        res.json({ status: 1, data: brands })
    },
    async add(req, res) {
        let validator = new v(req.body, { brand_name: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)

        let save = await db.m_brand.create({
            brand_name: req.body.brand_name,
            statusid: 1
        })
        await validate(res, save, 'An error occured while saving brand\'s data. Please try again.')
        res.json({ status: 1 })
    },
    async edit(req, res) {
        let validator = new v(req.body, { brand_name: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)
        
        req.params.id = hashids.decode(req.params.id)
        let find = await db.m_brand.findOne({
            attributes: ['id'],
            where: { id: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Brand is not found')
        
        let save = db.m_brand.update(
            { brand_name: req.body.brand_name },
            { where: { id: req.params.id, statusid: 1 } }
        )
        await validate(res, save, 'An error occured while saving brand\'s data. Please try again.')
        res.json({ status: 1 })
    },
    async del(req, res) {
        let validator = new v(req.params, { id: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)
        
        req.params.id = hashids.decode(req.params.id)
        let find = await db.m_brand.findOne({
            attributes: ['id'],
            where: { id: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Brand is not found')

        let save = db.m_brand.update(
            { statusid: 0 },
            { where: { id: req.params.id, statusid: 1 } }
        )
        await validate(res, save, 'An error occured while saving brand\'s data. Please try again.')
        res.json({ status: 1 })
    }
}