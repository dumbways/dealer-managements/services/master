`use strict`
const { Validator: v } = require('node-input-validator')
const { validate, commit, hashids } = require(`../helper/helper`)
const fs = require('fs');

module.exports = {
    async list(req, res) {
        let customer = await db.customer.findAll({
            attributes: ['id', 'name', 'email', 'address'],
            where: { statusid: 1 },
            raw: true
        })
        customer = hashids.encodeArray(customer, 'id')
        res.json({ status: 1, data: customer })
    },
    async add(req, res) {
        let validator = new v(req.body, { name: 'required', email: 'required|email', address: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)

        req.body.email = req.body.email.toLowerCase()
        let validateEmail = await db.customer.findOne({
            attributes: ['email'],
            where: { email: req.body.email, statusid: 1 },
            raw: true
        })
        await validate(res, !validateEmail, 'Email has been registered !')

        /** Save File (Image) */
        let filename = req.files.photo.filename.split('.')
        let photoName = `photo_${req.body.email}.${filename[filename.length - 1]}`
        fs.rename(req.files.photo.file, `${__basedir}/protected/customer/${photoName}`, async (err) => {
            /** Save Data */
            let save = await db.customer.create({
                name: req.body.name,
                email: req.body.email,
                address: req.body.address,
                photo: photoName,
                create_by: req.auth.username,
                statusid: 1
            })
            await validate(res, save, 'An error occured while saving customer\'s data. Please try again.')
            res.json({ status: 1 })
        })
    },
    async edit(req, res) {
        let validator = new v(req.body, { name: 'required', email: 'required|email', address: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)
        
        req.params.id = hashids.decode(req.params.id)
        let find = await db.customer.findOne({
            attributes: ['id'],
            where: { id: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Customer is not found')

        req.body.email = req.body.email.toLowerCase()
        let validateEmail = await db.customer.findOne({
            attributes: ['email'],
            where: { id: { [Op.ne]: req.params.id }, email: req.body.email, statusid: 1 },
            raw: true
        })
        await validate(res, !validateEmail, 'Email has been registered !')
        
        /** Save File (Image) */
        let filename = req.files.photo.filename.split('.')
        let photoName = `photo_${req.body.email}.${filename[filename.length - 1]}`
        fs.rename(req.files.photo.file, `${__basedir}/protected/customer/${photoName}`, async (err) => {
            let save = await sequelize.transaction()
            let deactive = db.customer.update(
                { statusid: 0 },
                { where: { id: req.params.id, statusid: 1 } }
            )
            
            let create = await db.customer.create({
                name: req.body.name,
                email: req.body.email,
                address: req.body.address,
                photo: photoName,
                create_by: req.auth.username,
                statusid: 1
            })
    
            await commit(save, deactive && create)
            await validate(res, deactive && create, 'An error occured while saving customer\'s data. Please try again.')
            res.json({ status: 1 })
        })
    },
    async del(req, res) {
        let validator = new v(req.params, { id: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)
        
        req.params.id = hashids.decode(req.params.id)
        let find = await db.customer.findOne({
            attributes: ['email', 'name', 'address'],
            where: { id: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Customer is not found')

        let save = db.customer.update(
            { statusid: 0 },
            { where: { id: req.params.id, statusid: 1 } }
        )
        await validate(res, save, 'An error occured while saving customer\'s data. Please try again.')
        res.json({ status: 1 })
    },
    async image(req, res) {
        req.params.id = hashids.decode(req.params.id)
        let customer = await db.customer.findOne({
            attributes: [ 'photo' ],
            where: { id: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, customer, 'customer is not found')
        res.sendFile(`${__basedir}/protected/customer/${customer.photo}`)
    }
}