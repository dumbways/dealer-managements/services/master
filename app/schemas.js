'use strict';
module.exports = () => {
    db.s_label_menu.hasMany(db.s_menu, {foreignKey : 'id'});
    db.s_menu.belongsTo(db.s_label_menu, {foreignKey : 'label_id'});

    db.car.belongsTo(db.m_brand, { as: 'mb', foreignKey : 'brand_id'});
    db.m_brand.hasMany(db.car, { as: 'c', foreignKey : 'id'});
}
