'use strict';
const authenticate  = require(`${__basedir}/app/middleware/authenticate`)
const authorize     = require(`${__basedir}/app/middleware/authorize`)

module.exports = (router) => {
    router.post('/authenticate', (req, res) => { authenticate(req, res) })
    router.get('/userinfo', 'userinfo', [authorize], (req, res) => {
        models.main.userinfo(req, res)
    })
    router.get('/customer', 'customer', [authorize], (req, res) => {
        models.customer.list(req, res)
    })
    router.post('/customer', 'customer', [authorize], (req, res) => {
        models.customer.add(req, res)
    })
    router.put('/customer/:id', 'customer', [authorize], (req, res) => {
        models.customer.edit(req, res)
    })
    router.delete('/customer/:id', 'customer', [authorize], (req, res) => {
        models.customer.del(req, res)
    })
    router.get('/customer/image/:id', 'cars', [authorize], (req, res) => {
        models.customer.image(req, res)
    })
    
    router.get('/brands', 'brands', [authorize], (req, res) => {
        models.brands.list(req, res)
    })
    router.post('/brands', 'brands', [authorize], (req, res) => {
        models.brands.add(req, res)
    })
    router.put('/brands/:id', 'brands', [authorize], (req, res) => {
        models.brands.edit(req, res)
    })
    router.delete('/brands/:id', 'brands', [authorize], (req, res) => {
        models.brands.del(req, res)
    })

    router.get('/cars', 'cars', [authorize], (req, res) => {
        models.cars.list(req, res)
    })
    router.post('/cars', 'cars', [authorize], (req, res) => {
        models.cars.add(req, res)
    })
    router.delete('/cars/:id', 'cars', [authorize], (req, res) => {
        models.cars.del(req, res)
    })
    router.get('/cars/:id', 'cars', [authorize], (req, res) => {
        models.cars.view(req, res)
    })
    router.get('/cars/image/:id', 'cars', [authorize], (req, res) => {
        models.cars.image(req, res)
    })
    // router.put('/cars/:id', 'cars', [authorize], (req, res) => {
    //     models.cars.edit(req, res)
    // })
}