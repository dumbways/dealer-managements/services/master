`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`car`, {
        brand_id : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        car_name : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        image : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        color : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        description : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        stock : {
            type      : Sequelize.INTEGER,
            allowNull : false
        },
        create_at : {
            type      : Sequelize.DATE,
            allowNull : false,
            defaultValue: Sequelize.fn('NOW')
        },
        create_by : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        statusid : {
            type      : Sequelize.SMALLINT,
            allowNull : false
        },
    }, {
        schema: 'skeleton',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    });
};