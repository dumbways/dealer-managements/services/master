`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`personal_data`, {
        username : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        name : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        address : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        photo : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        create_at : {
            type      : Sequelize.DATE,
            allowNull : false
        },
        statusid : {
            type      : Sequelize.SMALLINT,
            allowNull : false
        },
    }, {
        schema: 'skeleton',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    });
};