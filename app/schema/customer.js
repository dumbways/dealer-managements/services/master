`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`customer`, {
        name : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        email : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        address : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        photo : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        create_at : {
            type      : Sequelize.DATE,
            allowNull : false,
            defaultValue: Sequelize.fn('NOW')
        },
        create_by : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        statusid : {
            type      : Sequelize.SMALLINT,
            allowNull : false
        },
    }, {
        schema: 'skeleton',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    });
};