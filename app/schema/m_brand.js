`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`m_brand`, {
        brand_name : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        statusid : {
            type      : Sequelize.SMALLINT,
            allowNull : false
        },
    }, {
        schema: 'skeleton',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    });
};