`use strict`
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const {validate} = require(`${__basedir}/app/helper/helper`)

const authenticate = async (req, res) => {
    bcrypt.hash(req.body.password, env.app.secret, async (error, hash) => {
        await validate(res, !error, error)

        req.body.password = hash
        let user = await db.authentication_user.findOne({ attributes: ['username'], where: { username: req.body.username, password: req.body.password, statusid: 1 } })
        await validate(res, user, 'Unauthorized', 403)

        const token = jwt.sign({username: user.username}, env.app.secret)
        let session = await db.authentication_user_login.create({ username: user.username, token: token })
        await validate(res, session, 'Something went wrong. Please try again later.')

        res.json({ status: 1, data: token })
    })
}

module.exports = authenticate