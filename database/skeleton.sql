--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

-- Started on 2020-03-21 23:11:20

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 10 (class 2615 OID 16410)
-- Name: skeleton; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA skeleton;


ALTER SCHEMA skeleton OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 226 (class 1259 OID 24673)
-- Name: car; Type: TABLE; Schema: skeleton; Owner: postgres
--

CREATE TABLE skeleton.car (
    id integer NOT NULL,
    car_name character varying(100) NOT NULL,
    brand_id integer NOT NULL,
    image text NOT NULL,
    color character varying(30) NOT NULL,
    description text NOT NULL,
    create_at timestamp without time zone NOT NULL,
    create_by character varying(100) NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL,
    stock integer NOT NULL
);


ALTER TABLE skeleton.car OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 24671)
-- Name: car_id_seq; Type: SEQUENCE; Schema: skeleton; Owner: postgres
--

CREATE SEQUENCE skeleton.car_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE skeleton.car_id_seq OWNER TO postgres;

--
-- TOC entry 2902 (class 0 OID 0)
-- Dependencies: 225
-- Name: car_id_seq; Type: SEQUENCE OWNED BY; Schema: skeleton; Owner: postgres
--

ALTER SEQUENCE skeleton.car_id_seq OWNED BY skeleton.car.id;


--
-- TOC entry 222 (class 1259 OID 24652)
-- Name: customer; Type: TABLE; Schema: skeleton; Owner: postgres
--

CREATE TABLE skeleton.customer (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    email character varying(100) NOT NULL,
    address character varying(255) NOT NULL,
    create_at timestamp without time zone NOT NULL,
    create_by character varying(100) NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL
);


ALTER TABLE skeleton.customer OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 24650)
-- Name: customer_id_seq; Type: SEQUENCE; Schema: skeleton; Owner: postgres
--

CREATE SEQUENCE skeleton.customer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE skeleton.customer_id_seq OWNER TO postgres;

--
-- TOC entry 2903 (class 0 OID 0)
-- Dependencies: 221
-- Name: customer_id_seq; Type: SEQUENCE OWNED BY; Schema: skeleton; Owner: postgres
--

ALTER SEQUENCE skeleton.customer_id_seq OWNED BY skeleton.customer.id;


--
-- TOC entry 224 (class 1259 OID 24664)
-- Name: m_brand; Type: TABLE; Schema: skeleton; Owner: postgres
--

CREATE TABLE skeleton.m_brand (
    id integer NOT NULL,
    brand_name character varying(50) NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL
);


ALTER TABLE skeleton.m_brand OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 24662)
-- Name: m_brand_id_seq; Type: SEQUENCE; Schema: skeleton; Owner: postgres
--

CREATE SEQUENCE skeleton.m_brand_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE skeleton.m_brand_id_seq OWNER TO postgres;

--
-- TOC entry 2904 (class 0 OID 0)
-- Dependencies: 223
-- Name: m_brand_id_seq; Type: SEQUENCE OWNED BY; Schema: skeleton; Owner: postgres
--

ALTER SEQUENCE skeleton.m_brand_id_seq OWNED BY skeleton.m_brand.id;


--
-- TOC entry 210 (class 1259 OID 16413)
-- Name: personal_data; Type: TABLE; Schema: skeleton; Owner: postgres
--

CREATE TABLE skeleton.personal_data (
    id integer NOT NULL,
    username character varying(50),
    name character varying(200),
    address character varying(200),
    create_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    create_by character varying(50),
    statusid smallint,
    photo text
);


ALTER TABLE skeleton.personal_data OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16411)
-- Name: personal_data_id_seq; Type: SEQUENCE; Schema: skeleton; Owner: postgres
--

CREATE SEQUENCE skeleton.personal_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE skeleton.personal_data_id_seq OWNER TO postgres;

--
-- TOC entry 2905 (class 0 OID 0)
-- Dependencies: 209
-- Name: personal_data_id_seq; Type: SEQUENCE OWNED BY; Schema: skeleton; Owner: postgres
--

ALTER SEQUENCE skeleton.personal_data_id_seq OWNED BY skeleton.personal_data.id;


--
-- TOC entry 2753 (class 2604 OID 24676)
-- Name: car id; Type: DEFAULT; Schema: skeleton; Owner: postgres
--

ALTER TABLE ONLY skeleton.car ALTER COLUMN id SET DEFAULT nextval('skeleton.car_id_seq'::regclass);


--
-- TOC entry 2749 (class 2604 OID 24655)
-- Name: customer id; Type: DEFAULT; Schema: skeleton; Owner: postgres
--

ALTER TABLE ONLY skeleton.customer ALTER COLUMN id SET DEFAULT nextval('skeleton.customer_id_seq'::regclass);


--
-- TOC entry 2751 (class 2604 OID 24667)
-- Name: m_brand id; Type: DEFAULT; Schema: skeleton; Owner: postgres
--

ALTER TABLE ONLY skeleton.m_brand ALTER COLUMN id SET DEFAULT nextval('skeleton.m_brand_id_seq'::regclass);


--
-- TOC entry 2747 (class 2604 OID 16416)
-- Name: personal_data id; Type: DEFAULT; Schema: skeleton; Owner: postgres
--

ALTER TABLE ONLY skeleton.personal_data ALTER COLUMN id SET DEFAULT nextval('skeleton.personal_data_id_seq'::regclass);


--
-- TOC entry 2896 (class 0 OID 24673)
-- Dependencies: 226
-- Data for Name: car; Type: TABLE DATA; Schema: skeleton; Owner: postgres
--

COPY skeleton.car (id, car_name, brand_id, image, color, description, create_at, create_by, statusid, stock) FROM stdin;
1	Toyota Brimo	2	Toyota Brimo_2	white	lalala	2020-03-21 13:53:46.957913	m.annafia	1	10
\.


--
-- TOC entry 2892 (class 0 OID 24652)
-- Dependencies: 222
-- Data for Name: customer; Type: TABLE DATA; Schema: skeleton; Owner: postgres
--

COPY skeleton.customer (id, name, email, address, create_at, create_by, statusid) FROM stdin;
1	adsfa	dsafas@adfas.com	asdfasdf	2020-03-21 10:14:15.108233	m.annafia	0
3	adsfaafds	dsafas@adfas.com	asdfasdf	2020-03-21 10:51:42.409269	m.annafia	0
5	customer 2	customer2@gmail.com	Malang Kota	2020-03-21 10:55:47.062884	m.annafia	1
6	customer 3	customer3@gmail.com	Surabaya	2020-03-21 10:56:26.246063	m.annafia	1
4	adsfaafdsssss	dsafas@adfas.comm	asdfasdfsafdas	2020-03-21 10:52:01.372769	m.annafia	0
\.


--
-- TOC entry 2894 (class 0 OID 24664)
-- Dependencies: 224
-- Data for Name: m_brand; Type: TABLE DATA; Schema: skeleton; Owner: postgres
--

COPY skeleton.m_brand (id, brand_name, statusid) FROM stdin;
2	Toyotaaa	1
1	Toyota	0
3	Avanza	0
4	Avanza	1
5	Kijang	1
\.


--
-- TOC entry 2890 (class 0 OID 16413)
-- Dependencies: 210
-- Data for Name: personal_data; Type: TABLE DATA; Schema: skeleton; Owner: postgres
--

COPY skeleton.personal_data (id, username, name, address, create_at, create_by, statusid, photo) FROM stdin;
1	m.annafia	Mochammad Annafia Aktafian	Dusun Jatianom	2020-01-02 01:05:16.165092	m.annafia	1	\N
\.


--
-- TOC entry 2906 (class 0 OID 0)
-- Dependencies: 225
-- Name: car_id_seq; Type: SEQUENCE SET; Schema: skeleton; Owner: postgres
--

SELECT pg_catalog.setval('skeleton.car_id_seq', 1, true);


--
-- TOC entry 2907 (class 0 OID 0)
-- Dependencies: 221
-- Name: customer_id_seq; Type: SEQUENCE SET; Schema: skeleton; Owner: postgres
--

SELECT pg_catalog.setval('skeleton.customer_id_seq', 6, true);


--
-- TOC entry 2908 (class 0 OID 0)
-- Dependencies: 223
-- Name: m_brand_id_seq; Type: SEQUENCE SET; Schema: skeleton; Owner: postgres
--

SELECT pg_catalog.setval('skeleton.m_brand_id_seq', 5, true);


--
-- TOC entry 2909 (class 0 OID 0)
-- Dependencies: 209
-- Name: personal_data_id_seq; Type: SEQUENCE SET; Schema: skeleton; Owner: postgres
--

SELECT pg_catalog.setval('skeleton.personal_data_id_seq', 1, true);


--
-- TOC entry 2762 (class 2606 OID 24682)
-- Name: car car_pk; Type: CONSTRAINT; Schema: skeleton; Owner: postgres
--

ALTER TABLE ONLY skeleton.car
    ADD CONSTRAINT car_pk PRIMARY KEY (id);


--
-- TOC entry 2758 (class 2606 OID 24661)
-- Name: customer customer_pk; Type: CONSTRAINT; Schema: skeleton; Owner: postgres
--

ALTER TABLE ONLY skeleton.customer
    ADD CONSTRAINT customer_pk PRIMARY KEY (id);


--
-- TOC entry 2760 (class 2606 OID 24670)
-- Name: m_brand m_brand_pk; Type: CONSTRAINT; Schema: skeleton; Owner: postgres
--

ALTER TABLE ONLY skeleton.m_brand
    ADD CONSTRAINT m_brand_pk PRIMARY KEY (id);


--
-- TOC entry 2756 (class 2606 OID 16421)
-- Name: personal_data personal_data_pkey; Type: CONSTRAINT; Schema: skeleton; Owner: postgres
--

ALTER TABLE ONLY skeleton.personal_data
    ADD CONSTRAINT personal_data_pkey PRIMARY KEY (id);


-- Completed on 2020-03-21 23:11:21

--
-- PostgreSQL database dump complete
--

