--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

-- Started on 2020-03-21 23:10:19

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 7 (class 2615 OID 16393)
-- Name: authentication; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA authentication;


ALTER SCHEMA authentication OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 220 (class 1259 OID 16460)
-- Name: authentication_api_role; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_api_role (
    id integer NOT NULL,
    apiname character varying(50) NOT NULL,
    rolename character varying(50) NOT NULL,
    statusid smallint NOT NULL
);


ALTER TABLE authentication.authentication_api_role OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16458)
-- Name: api_role_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.api_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.api_role_id_seq OWNER TO postgres;

--
-- TOC entry 2910 (class 0 OID 0)
-- Dependencies: 219
-- Name: api_role_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.api_role_id_seq OWNED BY authentication.authentication_api_role.id;


--
-- TOC entry 216 (class 1259 OID 16448)
-- Name: authentication_role; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_role (
    id integer NOT NULL,
    rolename character varying(50) NOT NULL,
    roledesc character varying(50) NOT NULL,
    statusid smallint NOT NULL
);


ALTER TABLE authentication.authentication_role OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16396)
-- Name: authentication_user; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_user (
    id integer NOT NULL,
    username character varying(50) NOT NULL,
    password character varying(200) NOT NULL,
    create_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    create_by character varying(50) NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL
);


ALTER TABLE authentication.authentication_user OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 16404)
-- Name: authentication_user_login; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_user_login (
    id integer NOT NULL,
    token character varying(200) NOT NULL,
    username character varying(50) NOT NULL,
    create_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL
);


ALTER TABLE authentication.authentication_user_login OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 16454)
-- Name: authentication_user_role; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_user_role (
    id integer NOT NULL,
    username character varying(100) NOT NULL,
    rolename character varying(50) NOT NULL,
    create_by character varying(100),
    statusid smallint DEFAULT 1 NOT NULL,
    create_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE authentication.authentication_user_role OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16446)
-- Name: role_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.role_id_seq OWNER TO postgres;

--
-- TOC entry 2911 (class 0 OID 0)
-- Dependencies: 215
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.role_id_seq OWNED BY authentication.authentication_role.id;


--
-- TOC entry 205 (class 1259 OID 16394)
-- Name: user_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.user_id_seq OWNER TO postgres;

--
-- TOC entry 2912 (class 0 OID 0)
-- Dependencies: 205
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.user_id_seq OWNED BY authentication.authentication_user.id;


--
-- TOC entry 207 (class 1259 OID 16402)
-- Name: user_login_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.user_login_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.user_login_id_seq OWNER TO postgres;

--
-- TOC entry 2913 (class 0 OID 0)
-- Dependencies: 207
-- Name: user_login_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.user_login_id_seq OWNED BY authentication.authentication_user_login.id;


--
-- TOC entry 217 (class 1259 OID 16452)
-- Name: user_role_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.user_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.user_role_id_seq OWNER TO postgres;

--
-- TOC entry 2914 (class 0 OID 0)
-- Dependencies: 217
-- Name: user_role_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.user_role_id_seq OWNED BY authentication.authentication_user_role.id;


--
-- TOC entry 2758 (class 2604 OID 16463)
-- Name: authentication_api_role id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_api_role ALTER COLUMN id SET DEFAULT nextval('authentication.api_role_id_seq'::regclass);


--
-- TOC entry 2754 (class 2604 OID 16451)
-- Name: authentication_role id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_role ALTER COLUMN id SET DEFAULT nextval('authentication.role_id_seq'::regclass);


--
-- TOC entry 2748 (class 2604 OID 16399)
-- Name: authentication_user id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user ALTER COLUMN id SET DEFAULT nextval('authentication.user_id_seq'::regclass);


--
-- TOC entry 2751 (class 2604 OID 16407)
-- Name: authentication_user_login id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user_login ALTER COLUMN id SET DEFAULT nextval('authentication.user_login_id_seq'::regclass);


--
-- TOC entry 2755 (class 2604 OID 16457)
-- Name: authentication_user_role id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user_role ALTER COLUMN id SET DEFAULT nextval('authentication.user_role_id_seq'::regclass);


--
-- TOC entry 2904 (class 0 OID 16460)
-- Dependencies: 220
-- Data for Name: authentication_api_role; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

COPY authentication.authentication_api_role (id, apiname, rolename, statusid) FROM stdin;
1	userinfo	admin	1
2	customer	admin	1
3	brands	admin	1
4	cars	admin	1
\.


--
-- TOC entry 2900 (class 0 OID 16448)
-- Dependencies: 216
-- Data for Name: authentication_role; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

COPY authentication.authentication_role (id, rolename, roledesc, statusid) FROM stdin;
1	admin	Administrator	1
\.


--
-- TOC entry 2896 (class 0 OID 16396)
-- Dependencies: 206
-- Data for Name: authentication_user; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

COPY authentication.authentication_user (id, username, password, create_at, create_by, statusid) FROM stdin;
2	m.annafia	$2b$10$zixCcN86MZ.ZBpbxS6FNQeLjYunDvrxQNx4swD348IySaWJmNkp0O	2020-01-01 21:00:55.588406	m.annafia	1
\.


--
-- TOC entry 2898 (class 0 OID 16404)
-- Dependencies: 208
-- Data for Name: authentication_user_login; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

COPY authentication.authentication_user_login (id, token, username, create_at, statusid) FROM stdin;
2	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTMyNjE2NH0.87dwQUgfIz0NeAnz8Qa8XElTGe0N2CHAWUNe5V2IPtk	m.annafia	2020-02-10 09:16:04.427115	1
3	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTMyNjE4MH0.k_cCCkv75T0OcR9RlDMYVYGqrZIXf6YNeD6QRrna2aU	m.annafia	2020-02-10 09:16:20.434076	1
4	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTMzNTM4MH0.rjnzwIaB0oX2u7iuO2RQ7-TdSJEPDbsls2LyEV6c7Qk	m.annafia	2020-02-10 11:49:40.439613	1
5	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTMzNTQ1OH0.21E0mYCJ58IKj-8CW-FY7pKxT__iKk0OhFNuCu3zH3E	m.annafia	2020-02-10 11:50:58.412635	1
6	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTQ2ODAyMn0.H5tpcXOa-m-xjsDYMCc6pfyBHf11Ju9qQQNewSdGQkk	m.annafia	2020-02-12 00:40:22.105213	1
7	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTQ4ODY3Nn0.yNJgGI_AxmpX7xEr3E_Mh87kQA5IiukWznqnI8ttUa4	m.annafia	2020-02-12 06:24:36.315505	1
8	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTQ5Mjg1NH0.kGm6IDUK7-M4RK-ahPwsZgIcZNnGGBeQru8vSSYOqb8	m.annafia	2020-02-12 07:34:14.738626	1
9	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTQ5ODczOX0.Aor12LOCbcja5LXIeVt-cvdqWj7k9774ImqsFBBdghQ	m.annafia	2020-02-12 09:12:19.31453	1
10	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTUwMjY0N30.u0HxA79uo85LX6aVW5z-AnUpXTthW7ZEgkd8NN4dwYA	m.annafia	2020-02-12 10:17:27.360648	1
11	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTUzNTc4Nn0.D5-RYwMwJCFriL8YZqPRaCT5qVuYjkq4rMbsTgQSl34	m.annafia	2020-02-12 19:29:46.863947	1
12	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTYxODY0Mn0._2jwp5x6RjqnCCFE-kruXJM8fql15Ao-4wHCgUGc1hc	m.annafia	2020-02-13 18:30:42.0314	1
13	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTc4MjA2MH0.T5iQ6--bNjTf70QYE-SBcjCPvQj_h9EXgX6-KJao7YI	m.annafia	2020-02-15 15:54:20.784084	1
14	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTgwOTY0N30.OAzwNEZ31cytVICFEO7GH2u9NtMcMGNmD8LJUvQSKYU	m.annafia	2020-02-15 23:34:07.572592	1
15	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTgyNDkyMn0.klkVAc53V3QhXv7EuoBWOxNrM1NI9Pu-biLExH_W-go	m.annafia	2020-02-16 03:48:42.526526	1
16	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTg0MDA3OH0.3Tdf1zzwTLHUpq2Pj2WIcngNfQkS1Q-_lLdPo5UqS1s	m.annafia	2020-02-16 08:01:18.789881	1
17	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTg0NDc0MH0.mbuT0sKTNIRpsWL6y2T87JA8PdyvbMaxit9XtfQmWVc	m.annafia	2020-02-16 09:19:00.470471	1
18	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTg0NDkwNH0.EQOB5sT8V_SWC1MRPTle1lvleaPWoqvto8VgajL-uXU	m.annafia	2020-02-16 09:21:44.191358	1
19	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTg1NjkwN30.a8t2iWKgJ7jswcAy6cYU_ZEODr_nRY0tGPZc9GIxDCs	m.annafia	2020-02-16 12:41:47.54299	1
20	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc1Nzg1OX0.JBA-5Xz1iBZzoSkL15qQ3Lu9Z9IfmFAalszpJ2z0jSs	m.annafia	2020-03-21 02:30:59.185828	1
21	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc3MzIyNn0.8e_tjjZ8cUKGYXUmIYpBgxQQBuDcc7nd7UUX4V7l0LY	m.annafia	2020-03-21 06:47:06.754007	1
22	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc3NzQ0Nn0.4ywfaU3GPIgBiCefMhgUTwvZzFIDxW9Za1AJAUOOMcI	m.annafia	2020-03-21 07:57:26.522735	1
23	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc3NzQ3Mn0.VlVs559sx_u3-V6cKHhqfnJQJ18T34k_I_N7sLgat6A	m.annafia	2020-03-21 07:57:52.128012	1
24	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc3NzUyNH0.pMrpDCVHQnnuv2eB34UpK9W1F-GVUAZkjeAFMuXObWI	m.annafia	2020-03-21 07:58:44.907055	1
25	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc4NDM4NX0.lSP3NTOXF0Pnz_LVGLIsTruVKlvpLgILENSWX4F6M0g	m.annafia	2020-03-21 09:53:05.292814	1
26	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc4NDQyMH0.xFAB-l4pzfuUCgsgs8qZREUvBrD7dNPPsDopAucphiA	m.annafia	2020-03-21 09:53:40.265502	1
27	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc4NDQ2Mn0.mOUPBiQQSUvl1mXquoGV8GnvfMSZRzmVO8WKDx6iyqw	m.annafia	2020-03-21 09:54:22.678285	1
28	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc4ODExOX0.m0qs8aswm69nH-qW-1I88vEdvU-kp9FM1bNk1Nnb9h4	m.annafia	2020-03-21 10:55:19.32734	1
29	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc4OTkyMn0.kkUx7fspND9mb6whqu9NIZBhFLoCPw8hHa8ZkdnvBhI	m.annafia	2020-03-21 11:25:22.713984	1
30	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc5NDc3MX0.7_fZlzunW-yrSxJUlNs1_QfYWpGnZMKJaDyOEur3dsc	m.annafia	2020-03-21 12:46:11.434264	1
31	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc5ODc4N30.v6iIBd2M-StM6hUUvv5j1Tyghou_BxNRiQ7F69FDx1c	m.annafia	2020-03-21 13:53:07.336043	1
1	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU3Nzg5NjQyOH0.TTGGQEoTM4s_gtsdsl79lg2Tpf2NI5yLplSy-L5jpns	m.annafia	2020-01-03 06:22:32.245	1
\.


--
-- TOC entry 2902 (class 0 OID 16454)
-- Dependencies: 218
-- Data for Name: authentication_user_role; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

COPY authentication.authentication_user_role (id, username, rolename, create_by, statusid, create_at) FROM stdin;
1	m.annafia	admin	m.annafia	1	2020-02-15 23:12:32.452727
\.


--
-- TOC entry 2915 (class 0 OID 0)
-- Dependencies: 219
-- Name: api_role_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.api_role_id_seq', 4, true);


--
-- TOC entry 2916 (class 0 OID 0)
-- Dependencies: 215
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.role_id_seq', 1, true);


--
-- TOC entry 2917 (class 0 OID 0)
-- Dependencies: 205
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.user_id_seq', 2, true);


--
-- TOC entry 2918 (class 0 OID 0)
-- Dependencies: 207
-- Name: user_login_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.user_login_id_seq', 31, true);


--
-- TOC entry 2919 (class 0 OID 0)
-- Dependencies: 217
-- Name: user_role_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.user_role_id_seq', 1, true);


--
-- TOC entry 2768 (class 2606 OID 24644)
-- Name: authentication_api_role api_role_pk; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_api_role
    ADD CONSTRAINT api_role_pk PRIMARY KEY (id);


--
-- TOC entry 2764 (class 2606 OID 24646)
-- Name: authentication_role role_pk; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_role
    ADD CONSTRAINT role_pk PRIMARY KEY (id);


--
-- TOC entry 2762 (class 2606 OID 16409)
-- Name: authentication_user_login user_login_pkey; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user_login
    ADD CONSTRAINT user_login_pkey PRIMARY KEY (id);


--
-- TOC entry 2760 (class 2606 OID 16401)
-- Name: authentication_user user_pkey; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 2766 (class 2606 OID 24642)
-- Name: authentication_user_role user_role_pk; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user_role
    ADD CONSTRAINT user_role_pk PRIMARY KEY (id);


-- Completed on 2020-03-21 23:10:20

--
-- PostgreSQL database dump complete
--

