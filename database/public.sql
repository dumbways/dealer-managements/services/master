--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

-- Started on 2020-03-21 23:11:00

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2888 (class 0 OID 0)
-- Dependencies: 4
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 212 (class 1259 OID 16430)
-- Name: s_label_menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_label_menu (
    id integer NOT NULL,
    label character varying(50) NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public.s_label_menu OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16428)
-- Name: label_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.label_menu_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.label_menu_id_seq OWNER TO postgres;

--
-- TOC entry 2889 (class 0 OID 0)
-- Dependencies: 211
-- Name: label_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.label_menu_id_seq OWNED BY public.s_label_menu.id;


--
-- TOC entry 214 (class 1259 OID 16439)
-- Name: s_menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_menu (
    id integer NOT NULL,
    menu character varying(25) NOT NULL,
    label_id integer NOT NULL,
    parent_id integer,
    url character varying(50),
    icons character varying(50),
    statusid smallint DEFAULT 1 NOT NULL,
    sequence integer NOT NULL,
    rolename text[]
);


ALTER TABLE public.s_menu OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16437)
-- Name: menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.menu_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menu_id_seq OWNER TO postgres;

--
-- TOC entry 2890 (class 0 OID 0)
-- Dependencies: 213
-- Name: menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.menu_id_seq OWNED BY public.s_menu.id;


--
-- TOC entry 2745 (class 2604 OID 16433)
-- Name: s_label_menu id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_label_menu ALTER COLUMN id SET DEFAULT nextval('public.label_menu_id_seq'::regclass);


--
-- TOC entry 2747 (class 2604 OID 16442)
-- Name: s_menu id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_menu ALTER COLUMN id SET DEFAULT nextval('public.menu_id_seq'::regclass);


--
-- TOC entry 2880 (class 0 OID 16430)
-- Dependencies: 212
-- Data for Name: s_label_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_label_menu (id, label, statusid) FROM stdin;
2	Monitoring	1
3	Master Data	1
1	Statistics	1
4	Super Administrator	1
5	Account	1
\.


--
-- TOC entry 2882 (class 0 OID 16439)
-- Dependencies: 214
-- Data for Name: s_menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_menu (id, menu, label_id, parent_id, url, icons, statusid, sequence, rolename) FROM stdin;
2	List of Donation	2	\N	#/donation	mdi mdi-heart-pulse	0	2	{admin}
3	List of Campaign	3	\N	#/campaign	mdi mdi-scale-balance	0	4	{admin}
4	List of Category	3	\N	#/category	mdi mdi-tag-heart-outline	0	5	{admin}
1	Dashboard Administrator	1	\N	#/dashboard	mdi mdi-chart-arc	0	1	{admin}
5	List of Donnors	2	\N	#/donnors	mdi mdi-account-group-outline	0	3	{admin}
6	Account Management	4	\N	#/account-management	mdi mdi-account-multiple-plus-outline	0	6	{admin}
7	Edit Account	5	\N	#/edit-account	mdi mdi-account-edit	0	7	{admin}
9	Customer	2	\N	#/customer	mdi mdi-account-group-outline	1	1	{admin}
10	Cars	2	\N	#/cars	mdi-car-multiple	1	2	{admin}
11	Brands	3	\N	#/brands	mdi mdi-tag-heart-outline	1	3	{admin}
\.


--
-- TOC entry 2891 (class 0 OID 0)
-- Dependencies: 211
-- Name: label_menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.label_menu_id_seq', 5, true);


--
-- TOC entry 2892 (class 0 OID 0)
-- Dependencies: 213
-- Name: menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.menu_id_seq', 11, true);


--
-- TOC entry 2750 (class 2606 OID 16436)
-- Name: s_label_menu label_menu_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_label_menu
    ADD CONSTRAINT label_menu_pk PRIMARY KEY (id);


--
-- TOC entry 2752 (class 2606 OID 16445)
-- Name: s_menu menu_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_menu
    ADD CONSTRAINT menu_pk PRIMARY KEY (id);


-- Completed on 2020-03-21 23:11:00

--
-- PostgreSQL database dump complete
--

